﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

namespace armienti.core
{
	public class PointCalculatorTest
	{
		[Test]
		public void CalculateTotalPoints_Test()
		{
			var pointCalculator = new PointCalculator();
			var killedEnemies = 24;
			var killedVillagers = 3;
			var multiplier = 2;
			var expectedPoints = (24 * 100 + 2 * 500 - 3 * 100) * 2;
			
			var points = pointCalculator.CalculateTotalPoints(killedEnemies,killedVillagers,multiplier);
			
			Assert.That(points, Is.EqualTo(expectedPoints));
		}
	}
}

