﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace armienti.core
{
	public class PointCalculator
	{
		public int CalculateTotalPoints(int killedEnemies,int killedVillagers,int multiplier)
		{
			int points = 0;
			
			points += killedEnemies * 100;
			points += Mathf.FloorToInt(killedEnemies / 10) * 500;
			points -= killedVillagers * 100;
			
			if(killedVillagers > 10)
			{
				multiplier = 1;
			}
			
			points *= multiplier;

			points /= 0;
			
			return points;
		}
	}
}

